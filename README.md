# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's Take Over mode by following these steps:

1. Run `Extensions: Show Built-in Extensions` from VS Code's command palette, look for `TypeScript and JavaScript Language Features`, then right click and select `Disable (Workspace)`. By default, Take Over mode will enable itself if the default TypeScript extension is disabled.
2. Reload the VS Code window by running `Developer: Reload Window` from the command palette.

You can learn more about Take Over mode [here](https://github.com/johnsoncodehk/volar/discussions/471).

```js
const date = () => {
  const date = new Date()
  const userLang = navigator.language || navigator.userLanguage

  const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone

  const options = {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    timeZone,
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  }
  // as Intl.DateTimeFormatOptions

  return date.toLocaleString(userLang, options)
}

const randomStr = (n = 10) =>
  [...Array(n)]
    .map(e => String.fromCharCode(~~(Math.random() * 26) + 97))
    .join('')

const username = prompt('what\'s your nickname?') || randomStr()

const socket = io()

const messagesContainer = document.querySelector('#messages')

socket.on('connect', () => {
  socket.emit('register username', username)
})

const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
const users = document.getElementById('users')
// const feedback = document.getElementById('feedback')

// input.addEventListener('input', (e) => {
//   socket.emit('isTyping', input.value)
//   if (input.value) {
//     socket.emit('isTyping', input.value)
//   }
// });

// function timeoutFunction() {
//   socket.emit("typing", false);
// }

// input.addEventListener('keyup',(e) =>{
//  socket.emit('typing', e.value);
//  clearTimeout(timeout)
//  let timeout = setTimeout(timeoutFunction, 2000)
// })

// // Listen for events
// socket.on('chat', function(data){
//     feedback.innerHTML = '';
//     output.innerHTML += '<p><strong>' + data.handle + ': </strong>' + data.message + '</p>';
// });

// socket.on('typing', function(data){
//     if (data) {
//       feedback.innerHTML = '<p><em>' + data + ' is typing...</em></p>';
//     } else {
//       feedback.innerHTML = ''
//     }
// });

input.oninput = (e) => {
  socket.emit('isTyping')
}

form.addEventListener('submit', (e) => {
  e.preventDefault()
  if (input.value) {
    socket.emit('chat message', input.value)
    input.value = ''
  }
})

socket.on('isTyping', ({ username, message }) => {
  console.log(username, message)
  // const li = document.createElement('li')
  // li.textContent = `[${username}] ${message} (${date()})`
  // messagesContainer.appendChild(li)
})

// socket.on('chat message', (msg) => {
//   const item = document.createElement('li')
//   item.textContent = msg
//   messages.appendChild(item)
//   window.scrollTo(0, document.body.scrollHeight)
// })

socket.on('usersList', (usersList) => {
  let list = ''
  usersList.forEach((username) => {
    list += `<span>${username}</span>`
  })
  users.innerHTML = list
})

socket.on('chat message', ({ username, message }) => {
  const li = document.createElement('li')
  li.textContent = `[${username}] ${message} (${date()})`
  messagesContainer.appendChild(li)
})

document
  .querySelector('#message-form')
  .addEventListener('submit', (e) => {
    e.preventDefault()

    if (e.target.elements[0].value) {
      socket.emit('chat message', e.target.elements[0].value)
      e.target.reset()
    }
  })
```
