/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface Payload{
  value: number
  unit: string
  type: string
  name: string
}

interface Message {
  topic: string
  date: string
  message: Payload
}