// import io from 'socket.io'
import io from 'socket.io-client'

export const useSocketIO = () => {
  const messages = ref<any[]>([])
  const socket = io('localhost:3000')

  const fetchMessages = () => {
    socket.on('MESSAGE', (data) => {
      messages.value = data
      // you can also do this.messages.push(data)
    })
  }

  const sendMessage = () => {
    socket.emit('SEND_MESSAGE', {
      user: Math.random().toString(36).slice(2, 7),
      message: Math.random().toString(36).slice(2, 7),
    })
    fetchMessages()
  }

  const clear = () => {
    socket.emit('CLEAR_MESSAGE')
    fetchMessages()
  }

  const led = () => {
    socket.emit('LED')
  }

  const ledOff = () => {
    socket.emit('LED_OFF')
  }

  return {
    messages,
    socket,
    fetchMessages,
    sendMessage,
    clear,
    led,
    ledOff,
  }
}
