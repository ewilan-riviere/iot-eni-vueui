import { createApp } from 'vue'
import './style.css'
import VueSocketIO from 'vue-3-socket.io'
import App from './App.vue'

// const socketIo = new VueSocketIO({
//   debug: true,
//   connection: 'http://metinseylan.com:1992',
//   // vuex: {
//   //   store,
//   //   actionPrefix: 'SOCKET_',
//   //   mutationPrefix: 'SOCKET_',
//   // },
//   options: { path: '/my-app/' }, // Optional options
// })

createApp(App)
  // .use(socketIo)
  .mount('#app')
